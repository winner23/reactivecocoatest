//
//  AppDelegate.h
//  ReactiveCocoaTest
//
//  Created by Volodymyr Vinyarskyy on 19.01.2018.
//  Copyright © 2018 Volodymyr Vinyarskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

