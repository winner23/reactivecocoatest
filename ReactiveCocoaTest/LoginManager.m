//
//  LoginManager.m
//  ReactiveCocoaTest
//
//  Created by Volodymyr Vinyarskyy on 21.01.2018.
//  Copyright © 2018 Volodymyr Vinyarskyy. All rights reserved.
//

#import "LoginManager.h"

@interface LoginManager()
@property (weak, nonatomic) NSString *username;
@property (weak, nonatomic) NSString *password;
@end

@implementation LoginManager

//@synthesize loggingIn;
+ (id)sharedManager {
    static LoginManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        self.loggingIn = NO;
        [RACObserve(self, username) subscribeNext: ^(NSString *newName) {
            NSLog(@"%@", newName);
        }];
        [RACObserve(self, password) subscribeNext: ^(NSString *newPassword){
            NSLog(@"Password is changed!");
        }];
    }
    return self;
}

- (id)logInWithUsername: (NSString *) username password: (NSString *) password {
    self.username = username;
    self.password = password;
    self.loggingIn = YES;
    return self;
}
- (NSString *) getLogginedUsername {
    return self.username;
}
- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

@end
