//
//  LoginManager.h
//  ReactiveCocoaTest
//
//  Created by Volodymyr Vinyarskyy on 21.01.2018.
//  Copyright © 2018 Volodymyr Vinyarskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface LoginManager : NSObject
@property (assign, nonatomic) Boolean loggingIn;

+ (id)sharedManager;
- (id)logInWithUsername: (NSString *) username password: (NSString *) password;
- (NSString *) getLogginedUsername;
@end
