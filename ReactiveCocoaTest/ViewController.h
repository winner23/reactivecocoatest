//
//  ViewController.h
//  ReactiveCocoaTest
//
//  Created by Volodymyr Vinyarskyy on 19.01.2018.
//  Copyright © 2018 Volodymyr Vinyarskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *logInButton;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordConfirmTextField;
@property (weak, nonatomic) IBOutlet UILabel *lable;
@property (weak, nonatomic) NSString *username;

@end

