//
//  ViewController.m
//  ReactiveCocoaTest
//
//  Created by Volodymyr Vinyarskyy on 19.01.2018.
//  Copyright © 2018 Volodymyr Vinyarskyy. All rights reserved.
//

#import "ViewController.h"
#import "ReactiveCocoa/ReactiveCocoa.h"
#import "ReactiveCocoa/RACEXTScope.h"
#import "LoginManager.h"

@interface ViewController ()
@property(assign, nonatomic) Boolean isLoggedIn;
@property(assign, nonatomic) Boolean isCreateEnabled;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reactiveInitialization];
}
 
- (void)reactiveInitialization {
    NSString * const UserDidLogOutNotification = @"UserDidLogOutNotification";
    LoginManager *loginManager = [LoginManager sharedManager];
    
    @weakify(self);
    RAC(self, isCreateEnabled) = [RACSignal combineLatest:@[
                                                            self.passwordTextField.rac_textSignal,
                                                            self.passwordConfirmTextField.rac_textSignal]
                                                   reduce:^(NSString *password, NSString *passwordConfirmarion) {
                                                       return @([password isEqualToString:passwordConfirmarion]);
                                                   }];
    RAC(self.logInButton, enabled) = [RACSignal combineLatest:@[
                    self.textField.rac_textSignal,
                    self.passwordTextField.rac_textSignal,
                    self.passwordConfirmTextField.rac_textSignal,
                    RACObserve(loginManager, loggingIn),
                    RACObserve(self, isLoggedIn)
            ] reduce:^(NSString *username, NSString *password, NSString *passwordConfirmation, NSNumber *loggingIn, NSNumber *loggedIn) {
                if (password.length > 0 && passwordConfirmation.length >0) {
                    if (self.isCreateEnabled) {
                        [self.lable setHidden: YES];
                    } else {
                        [self.lable setHidden: NO];
                        self.lable.text = @"Password not suitable";
                        self.lable.textColor = UIColor.redColor;
                    }
                }
                return @(username.length > 0 && password.length > 0 && !loggingIn.boolValue && !loggedIn.boolValue && self.isCreateEnabled);
            }];
    [[self.logInButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *sender) {
        @strongify(self);
        [loginManager logInWithUsername:self.textField.text password:self.passwordTextField.text];
        
    }];
    RAC(self, isLoggedIn) = [[NSNotificationCenter.defaultCenter
                            rac_addObserverForName: UserDidLogOutNotification object:nil]
                           mapReplace:@NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
